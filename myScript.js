function validateForm() {
	var q1 = document.forms["myForm"]["q1"].value;
	var q2 = document.forms["myForm"]["q2"].value;
	var q3 = document.forms["myForm"]["q3"].value;
	var q4 = document.forms["myForm"]["q4"].value;
	var q5 = document.forms["myForm"]["q5"].value;
	var q6 = document.forms["myForm"]["q6"].value;
	var q7 = document.forms["myForm"]["q7"].value;
	var q8 = document.forms["myForm"]["q8"].value;
	var q9 = document.forms["myForm"]["q9"].value;
	var q10 = document.forms["myForm"]["q10"].value;
	var name = document.forms["myForm"]["name"].value;
	var score = 0;
	
  if (q1 == "") {
    alert("Oops! Question 1 is required");
    return false;
  }

  if (q2 == "") {
    alert("Oops! Question 2 is required");
    return false;
  }

  if (q3 == "") {
    alert("Oops! Question 3 is required");
    return false;
  }

  if (q4 == "") {
    alert("Oops! Question 4 is required");
    return false;
  }

  if (q5 == "") {
    alert("Oops! Question 5 is required");
    return false;
  }

  if (q6 == "") {
    alert("Oops! Question 6 is required");
    return false;
  }

  if (q7 == "") {
    alert("Oops! Question 7 is required");
    return false;
  }

  if (q8 == "") {
    alert("Oops! Question 8 is required");
    return false;
  }

  if (q9 == "") {
    alert("Oops! Question 9 is required");
    return false;
  }

  if (q10 == "") {
    alert("Oops! Question 10 is required");
    return false;
  }

  if (q1=="Id") {
  	score = score + 1;
  }

  if (q2=="clear") {
  	score = score + 1;
  }

  if (q3=="border-style") {
  	score = score + 1;
  }

  if (q4=="100%") {
  	score = score + 1;
  }

  if (q5=="class") {
  	score = score + 1;
  }

  if (q6=="/* a comment */") {
  	score = score + 1;
  }

  if (q7=="padding") {
  	score = score + 1;
  }

  if (q8=="font-size") {
  	score = score + 1;
  }

  if (q9=="p {color: red;}") {
  	score = score + 1;
  }

  if (q10=="a {text-decoration:none;}") {
  	score = score + 1;
  }

  if (score >= 0 && score <= 4) {
    alert("Keep trying, " + name + "!" + " You answered " + score + " out of 10 correctly");
    return score;
  }
  else if (score >= 5 && score <= 9) {
  	alert("Way to go, " + name + "!" + " You got " + score + " out of 10 correct");
  	return score;
  }
  else if (score == 10) {
  	alert("Congratulations " + name + "! You got 10 out of 10");
  	return score;
  }
}
